(function ($) {
    'use strict';

    var sileks_window = $(window);

    sileks_window.on('load', function () {
        $('#preloader').fadeOut(1500, function () {
            $(this).remove();
        });
    });
})(jQuery);