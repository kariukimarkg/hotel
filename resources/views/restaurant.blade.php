<!DOCTYPE html>
<html>
<head>
	<title>Ресторан | Хотел Силекс</title>

    @include('includes.head')


    



</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->

    @include('includes.navbar')

    <!-- Restaurant section -->

    <div class="container-fluid">
        <div class="row restaurant-header">
            <div class= "restaurant-header-overlay">
                <div class="container">
                    <h1 class="header-text">Ресторан</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid restaurant-section">
        <div class="row">
            <div class="col-md-6" data-aos="fade-up" data-aos-duration="1000" style="padding: 0">          
                <img src="{{asset('app/images/restaurant1.jpg')}}"  style="width:100%;">
            </div>
            <div class="col-md-6 restaurant-description" data-aos="fade-up" data-aos-duration="2000">
                <h2>Крем сала</h2>
                <p>Вкусете ги нашите национални и интернационални специјалитети приготвени од врвни мајстори во прекрасен амбиент.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 restaurant-description"  data-aos="fade-up" data-aos-duration="3000">
                <h2>Аперитив Бар</h2>
                <p>Аперитив барот во Хотел Силекс е идеално место за средби нудејќи Ви голем избор на локални и интернационални алкохолни и безалкохолни пијалоци, како и вкусно приготвена ужина.</p>
            </div>
            <div class="col-md-6" style="padding: 0" data-aos="fade-up" data-aos-duration="2000">                  
              <img src="{{asset('app/images/aperitiv1.jpg')}}"  style="width:100%;">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" data-aos="fade-up" data-aos-duration="1500" style="padding: 0">          
                <img src="{{asset('app/images/letna.jpg')}}"  style="width:100%;">
            </div>
            <div class="col-md-6 restaurant-description" data-aos="fade-up" data-aos-duration="2000">
                <h2>Летна тераса</h2>
                <p>Летната тераса е идеално место за да се поминат жешките летни денови со освежителен коктел а во подоцните часови вкусувајќи некој од прекрасните традиционални специјалитети од нашето мени.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 restaurant-description"  data-aos="fade-up" data-aos-duration="3000">
                <h2>Свадби и свечености</h2>
                <p>Вие сакате незаборавна и единствена венчавка што ќе ви остави илјадници спомени… Нашиот сектор за венчавки ќе ви помогне да Ви се оствари сонот!.</p>
            </div>
            <div class="col-md-6" style="padding: 0" data-aos="fade-up" data-aos-duration="2000">                  
              <img src="{{asset('app/images/svadbi.jpg')}}"  style="width:100%;">
            </div>
        </div>
    </div>


    <!-- Footer -->

    @include('includes.footer')


    <!-- Script preloader -->
    
	<script src="{{asset('app/js/preloader.js')}}"> </script>


    {{-- Script scroll fade in --}}

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>
</html>