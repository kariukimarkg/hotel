<!DOCTYPE html>
<html>
<head>
    <title>Хотел Силекс</title>

    @include('includes.head')

</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->

    @include('includes.navbar')

    <!-- Carousel -->

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item item-1 active ">
                <div class="item-overlay">
                </div>
            </div>
          <div class="item item-2">
                <div class="item-overlay">
                </div>
            </div>
            <div class="item item-3">
                <div class="item-overlay">
                </div>
            </div>    
            <div class="item item-4">
                <div class="item-overlay">
                </div>
            </div>  
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <!-- Carousel filter -->

    <section class="filter-area">
        <div class="hotel-search-form-area">
            <div class="container-fluid form-container">
                <div class="hotel-search-form">
                    <form method="POST" action="{{route('contact')}}"> 
                        {{ csrf_field() }}
                        <div class="row justify-content-between align-items-end filter-div">
                            <div class="col-md-4 col-sm-6">
                                <label for="checkIn">Од</label>
                                <input type="date" class="form-control" id="checkIn" name="checkin">
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <label for="checkOut">До</label>
                                <input type="date" class="form-control" id="checkOut" name="checkout">
                            </div>
                            <div class="col-md-1 col-sm-6">
                                <label for="adults">Лица</label>
                                <select name="adults" id="adults" class="form-control">
                                    <option value="" disabled selected class="first-option">/</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-6 check-button">
                                <label>/</label>
                                <button type="submit" class="form-control btn check-avability">Провери достапност</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- About section -->

    <div class="container about-us">
        <div class="row">
            <div class="col-md-6">
                <div class="section-heading">
                    <h2>Добредојдовте во Хотел Силекс</h2>
                </div>
                <div class="about-us-content">
                    <p>Хотел Силекс се наоѓа во туристичкиот комплекс Св.Стефан, оддалечен 4км од центарот на Охрид, на регионалниот патен правец Охрид – Св.Наум и 10км од аеродромот Св.Апостол Павле. <br><br> Хотел Силекс се простира на самиот брег на Охридското Езеро и се наоѓа помеѓу пространството на бистра сина езерска вода и зеленилото на боровата шума низ која поминува шумска уредена патека која Ве води до манастирот Св.Стефан на надморска висина од 720 м. <br><br>Ваквата локација е одлична можност за соединување на планинскиот и езерскиот туризам.</p>
                </div>
            </div>

            <div class="col-md-6 about-us-thumbnail">
                <div class="row">
                    <div class="col-md-6 col-sm-6 about-us-left">
                        <div class="single-thumb">
                            <img src="{{asset('app/images/13.jpg')}}" alt="">
                        </div>
                        <div class="single-thumb">
                            <img src="{{asset('app/images/14.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 about-us-right" >
                        <div class="single-thumb">
                            <img src="{{asset('app/images/15.jpg')}}"" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Services section -->

    <div class="services service-area">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-bed"></i>
                        </div>
                        <h3>Удобни соби</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-utensils"></i>
                        </div>
                        <h3>Ресторан</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-microphone-alt"></i>
                        </div>
                        <h3>Семинари</h3>
                    </div>
                </div>
                <div class=" col-md-2 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-glass-martini"></i>
                        </div>
                        <h3>Бар</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-swimming-pool"></i>
                        </div>
                        <h3>Базен</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-spa"></i>
                        </div>
                        <h3>Спа</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Accommodation section -->

    <div class="container">
        <div class="accommodation-section-title text-center">
            <h2>Сместување</h2>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6" data-aos="fade-up" data-aos-duration="2000">
                <div class="accommodation">
                    <div class="accommodation-photo accommodation-photo-cover">
                        <img src="{{asset('app/images/sobi.jpg')}}">
                    </div>
                    <div class="accommodation-detail">
                        <h3>                                
                            <a href="{{route('rooms')}}">Соби</a>
                        </h3>
                        <p>
                           <span><i class="fas fa-check"></i>Бања</span>
                           <span><i class="fas fa-check"></i>Мини бар</span>
                           <span><i class="fas fa-check"></i>Тераса</span>
                           <span><i class="fas fa-check"></i>LCD Телевизор</span>
                           <span><i class="fas fa-check"></i>Бесплатен WI-FI</span>
                        </p>
                        <div class="accommodation-read-more">
                            <a href="{{route('rooms')}}" class="btn btn-primary">Прочитај повеќе</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6" data-aos="fade-up" data-aos-duration="2500">
                <div class="accommodation">
                    <div class="accommodation-photo accommodation-photo-cover">
                        <img src="{{asset('app/images/apartmani.jpg')}}">
                    </div>
                    <div class="accommodation-detail">
                        <h3>                                
                            <a href="{{route('apartments')}}">Апартмани</a>
                        </h3>
                        <p>
                           <span><i class="fas fa-check"></i>Кујна</span>
                           <span><i class="fas fa-check"></i>Дневна</span>
                           <span><i class="fas fa-check"></i>Спална</span>
                           <span><i class="fas fa-check"></i>Нарачано будење</span>
                           <span><i class="fas fa-check"></i>Собна услуга</span>
                        </p>
                        <div class="accommodation-read-more">
                            <a href="{{route('apartments')}}" class="btn btn-primary">Прочитај повеќе</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6" data-aos="fade-up" data-aos-duration="3000">
                <div class="accommodation">
                    <div class="accommodation-photo accommodation-photo-cover">
                        <img src="{{asset('app/images/pretsedatelski.jpg')}}">
                    </div>
                    <div class="accommodation-detail">
                        <h3>                                
                            <a href="{{route('presidential-suite')}}">Претседателски Апартман</a>
                        </h3>
                        <p>
                           <span><i class="fas fa-check"></i>Две спални соби</span>
                           <span><i class="fas fa-check"></i>Кујна</span>
                           <span><i class="fas fa-check"></i>Дневна</span>
                           <span><i class="fas fa-check"></i>Тераса</span>
                           <span><i class="fas fa-check"></i>Собна услуга</span>
                        </p>
                        <div class="accommodation-read-more">
                            <a href="{{route('presidential-suite')}}" class="btn btn-primary">Прочитај повеќе</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->

    @include('includes.footer')

    {{-- Preloader --}}

    <script src="{{asset('app/js/preloader.js')}}"> </script>

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>
</html>