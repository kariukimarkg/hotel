<!DOCTYPE html>
<html>
<head>
	<title>Анимации | Хотел Силекс</title>

    @include('includes.head')


    



</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->

    @include('includes.navbar')



    <!-- Animations section -->

    <div class="container-fluid">
        <div class="row animation-header">
            <div class= "animation-header-overlay">
                <div class="container">
                    <h1 class="header-text">Анимации</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid animation-section">
        <div class="row" data-aos="fade-up" data-aos-duration="1000">
            <div class="col-md-6" style="padding: 0">          
                <img src="{{asset('app/images/bazen.jpg')}}"  style="width:100%;">
            </div>
            <div class="col-md-6 animation-description" data-aos="fade-up" data-aos-duration="2000">
                <h2>Базен</h2>
                <p>Главна атракција на Хотел Силекс е базенот на отворено, каде гостите имаат можност да уживаат во најразлични анимативни содржини во текот на денот и вечерните часови: игри во вода, концерти на музички ансамбли и театарски групи, избор на летни убавици, и сето тоа збогатено со послужување на храна и пијалоци на највисоко ниво.</p>
            </div>
        </div>
        <div class="row" data-aos="fade-up" data-aos-duration="2000">
            <div class="col-md-6 animation-description">
                <h2>Фитнес</h2>
                <p>Доколку сакатe да го подобрите Вашиот физички изглед , да ја надоградите вашата издржливост или едноставно да се одржувате во форма, нашата фитнес соба е секогаш спремна за вас и е бесплатна за користење.</p>
            </div>
            <div class="col-md-6" style="padding: 0">                  
              <img src="{{asset('app/images/fitness.jpg')}}"  style="width:100%;">
            </div>
        </div>
        <div class="row" data-aos="fade-up" data-aos-duration="3000">
            <div class="col-md-6"  style="padding: 0">          
                <img src="{{asset('app/images/sauna.jpg')}}"  style="width:100%;">
            </div>
            <div class="col-md-6 animation-description">
                <h2>Сауна</h2>
                <p>Oсвежете се во нашата сауна подобрувајќи го своето здравје додека сте наши гости.</p>
            </div>
        </div>
        <div class="row" data-aos="fade-up" data-aos-duration="2000">
            <div class="col-md-6 animation-description">
                <h2>Масажа</h2>
                <p>За врвна релаксација Ви нудиме различни видови на професионални масажи, и тоа во удобноста и приватноста на Вашата соба или во хотелскиот салон за масажа.</p>
            </div>
            <div class="col-md-6" style="padding: 0">                  
              <img src="{{asset('app/images/masaza.jpg')}}"  style="width:100%;">
            </div>
        </div>
    </div>


    <!-- Footer -->

    @include('includes.footer')


    <!-- Script preloader -->
    
	<script src="{{asset('app/js/preloader.js')}}"> </script>


    {{-- Script scroll fade in --}}

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>
</html>