<!DOCTYPE html>
<html>
<head>
	<title>Seminars | Hotel Sileks</title>

    @include('includes.head-en')    



</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->

    @include('includes.navbar-en')

    <!-- Seminars Header -->

    <div class="container-fluid">
        <div class="row seminar-header">
            <div class= "seminar-header-overlay">
                <div class="container">
                    <h1 class="header-text">Seminars</h1>
                </div>
            </div>
        </div>
    </div>


    <!-- Seminar Details -->

    <div class="container seminars-details">
        <div class="row">
            <div class="col-md-12 seminar-description">
                <h1>Conference Hall</h1>
                <p>With exceptional banquet and meeting facilities and impeccable service standard, we look forward to host your meetings in one of our 4 conference halls with total capacity from 30-200 seats. Supportive staff from the Sector for conferences at Sileks Hotel is always ready to quickly respond to your every request.<br>For successful realization of your seminar or congress, the hotel offers complete technical equipment offered by our hotel: Over-head projector, Projection screen, Flip chart, Free of charge WI-Fi, Scanner, Printer.</p>

            </div>
        </div>
        <div class="row table-forms">
            <div class="col-md-6 col-sm-12" data-aos="fade-up" data-aos-duration="1000">
                <img src="{{asset('app/images/p-forma1.jpg')}}">
                <h2>P-form: 150 persons</h2>
            </div>
            <div class="col-md-6 col-sm-12" data-aos="fade-up" data-aos-duration="2000">
                <img src="{{asset('app/images/kvadrat1.jpg')}}">
                <h2>Square: 65 persons</h2>
            </div>
        </div>
        <div class="row table-forms">
            <div class="col-md-6 col-sm-12" data-aos="fade-up" data-aos-duration="1000">
                <img src="{{asset('app/images/teatar1.jpg')}}">
                <h2>Theatre: 200 persons</h2>
            </div>
            <div class="col-md-6 col-sm-12" data-aos="fade-up" data-aos-duration="2000">
                <img src="{{asset('app/images/ucilnica1.jpg')}}">
                <h2>Classroom: 150 persons</h2>
            </div>
        </div>
    </div>


    <div class="container" style="margin-top: 100px">
        <div class="row">
            <div class="col-md-6">                  
              <div id="myCarousel" class="carousel slide" data-ride="carousel" data-aos="fade-up" data-aos-duration="1000">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active" style="height: auto;">
                        <img src="{{asset('app/images/kafepauza.jpg')}}"  style="width:100%;">
                    </div>
                    <div class="item" style="height: auto;">
                        <img src="{{asset('app/images/kafepauza1.jpg')}}"  style="width:100%;">
                    </div>
                
                    <div class="item" style="height: auto;">
                        <img src="{{asset('app/images/kafepauza2.jpg')}}" style="width:100%;">
                    </div>              
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-md-6 restaurant-description" data-aos="fade-up" data-aos-duration="2000">
                <h2>Coffee Break</h2>
                <p>Relax yourself after hard work session and make new connections with you colleges</p>
            </div>
        </div>
    </div>

    <!-- Footer -->

    @include('includes.footer-en')


    <!-- Script preloader -->
    
	<script src="{{asset('app/js/preloader.js')}}"> </script>


    {{-- Script scroll fade in --}}

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>
</html>