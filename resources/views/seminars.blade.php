<!DOCTYPE html>
<html>
<head>
	<title>Семинари | Хотел Силекс</title>

    @include('includes.head')


    



</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->

    @include('includes.navbar')


    <!-- Seminar Section -->

    
    <div class="container-fluid">
        <div class="row seminar-header">
            <div class= "seminar-header-overlay">
                <div class="container">
                    <h1 class="header-text">Семинари</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container seminars-details">
        <div class="row">
            <div class="col-md-12 seminar-description">
                <h1>Конференциски сали</h1>
                <p>Со исклучителни банкет и бизнис простории и беспрекорна услуга, ние со нетрпение очекуваме да ги организираме Вашите состаноци во некоја од нашите 4 конференциски сали со вкупен капацитет од 30-200 седишта. Подршката на персоналот од секторот за конференции на Хотел Силекс е секогаш подготвена брзо да одговори на секое Ваше барање.<br>За успешна реализација на вашиот семинар или конгрес хотелот нуди целосна техничка подршка и можност за користње на: LCD проектор, платно, Flip chart, WI-FI скенер, принтер</p>

            </div>
        </div>
        <div class="row table-forms">
            <div class="col-md-6 col-sm-12" data-aos="fade-up" data-aos-duration="1000">
                <img src="{{asset('app/images/p-forma1.jpg')}}">
                <h2>П-форма: 50 особи</h2>
            </div>
            <div class="col-md-6 col-sm-12" data-aos="fade-up" data-aos-duration="2000">
                <img src="{{asset('app/images/kvadrat1.jpg')}}">
                <h2>Квадрат: 65 особи</h2>
            </div>
        </div>
        <div class="row table-forms">
            <div class="col-md-6 col-sm-12" data-aos="fade-up" data-aos-duration="1000">
                <img src="{{asset('app/images/teatar1.jpg')}}">
                <h2>Театар: 200 особи</h2>
            </div>
            <div class="col-md-6 col-sm-12" data-aos="fade-up" data-aos-duration="2000">
                <img src="{{asset('app/images/ucilnica1.jpg')}}">
                <h2>Училница: 150 особи</h2>
            </div>
        </div>
    </div>


    <div class="container" style="margin-top: 100px">
        <div class="row">
            <div class="col-md-6">                  
              <div id="myCarousel" class="carousel slide" data-ride="carousel" data-aos="fade-up" data-aos-duration="1000">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active" style="height: auto;">
                        <img src="{{asset('app/images/kafepauza1.jpg')}}"  style="width:100%;">
                    </div>
                    <div class="item" style="height: auto;">
                        <img src="{{asset('app/images/kafepauza.jpg')}}"  style="width:100%;">
                    </div>
                
                    <div class="item" style="height: auto;">
                        <img src="{{asset('app/images/kafepauza2.jpg')}}" style="width:100%;">
                    </div>              
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-md-6 restaurant-description" data-aos="fade-up" data-aos-duration="2000">
                <h2>Кафе пауза</h2>
                <p>Опуштете се по напорната работна сесија и поврзете се со вашите колеги. Истото направете го во локациите наменети за кафе пауза како базенот, аперитив барот и лобито на хотелот.</p>
            </div>
        </div>
    </div>


    <!-- Footer -->

    @include('includes.footer')


    <!-- Script preloader -->
    
	<script src="{{asset('app/js/preloader.js')}}"> </script>


    {{-- Script scroll fade in --}}

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>
</html>