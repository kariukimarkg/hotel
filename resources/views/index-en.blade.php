<!DOCTYPE html>
<html>
<head>
    <title>Hotel Sileks</title>

    @include('includes.head-en')


</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->

    @include('includes.navbar-en')

    <!-- Carousel -->

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item item-1 active ">
                <div class="item-overlay">
                </div>
            </div>
          <div class="item item-2">
                <div class="item-overlay">
                </div>
            </div>
            <div class="item item-3">
                <div class="item-overlay">
                </div>
            </div>    
            <div class="item item-4">
                <div class="item-overlay">
                </div>
            </div>  
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    
        <!-- Carousel filter -->

    <section class="filter-area">
        <div class="hotel-search-form-area">
            <div class="container-fluid form-container">
                <div class="hotel-search-form">
                    <form method="POST" action="{{route('contact-en')}}"> 
                        {{ csrf_field() }}
                        <div class="row justify-content-between align-items-end filter-div">
                            <div class="col-md-4 col-sm-6">
                                <label for="checkIn">From</label>
                                <input type="date" class="form-control" id="checkIn" name="checkin">
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <label for="checkOut">To</label>
                                <input type="date" class="form-control" id="checkOut" name="checkout">
                            </div>
                            <div class="col-md-1 col-sm-6">
                                <label for="adults">Persons</label>
                                <select name="adults" id="adults" class="form-control">
                                    <option value="" disabled selected class="first-option">/</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-6 check-button">
                                <label>/</label>
                                <button type="submit" class="form-control btn check-avability">Check availability</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- About section -->

    <div class="container about-us">
        <div class="row">
            <div class="col-md-6">
                <div class="section-heading">
                    <h2>Welcome to Hotel Sileks</h2>
                </div>
                <div class="about-us-content">
                    <p>Hotel Sileks is located in tourist complex St.Stefan, 4 km away from Ohrid center, on the regional road Ohrid – St.Naum, and 10 km from Ohrid airport “St.Paul The Apostle”. <br><br> Hotel Sileks spreads on the shore of Lake Ohrid, placed between crystal blue lake water and greenery of pine forest ,where passes trail that leads to monastery St.Stefan -720 m altitude above sea level <br><br>Location like this is a great opportunity for union of mountain and lake tourism.</p>
                </div>
            </div>

            <div class="col-md-6 about-us-thumbnail">
                <div class="row">
                    <div class="col-md-6 col-sm-6 about-us-left">
                        <div class="single-thumb">
                            <img src="{{asset('app/images/13.jpg')}}" alt="">
                        </div>
                        <div class="single-thumb">
                            <img src="{{asset('app/images/14.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 about-us-right" >
                        <div class="single-thumb">
                            <img src="{{asset('app/images/15.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Services section -->

    <div class="services service-area">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-bed"></i>
                        </div>
                        <h3>Cosy rooms</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-utensils"></i>
                        </div>
                        <h3>Restaurant</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-microphone-alt"></i>
                        </div>
                        <h3>Seminars</h3>
                    </div>
                </div>
                <div class=" col-md-2 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-glass-martini"></i>
                        </div>
                        <h3>Bar</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-swimming-pool"></i>
                        </div>
                        <h3>Pool</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="service-info">
                        <div class="icon">
                            <i class="fas fa-spa"></i>
                        </div>
                        <h3>Sauna</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Accommodation section -->

    <div class="container">
        <div class="accommodation-section-title text-center">
            <h2>Accommodation</h2>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6" data-aos="fade-up" data-aos-duration="2000">
                <div class="accommodation">
                    <div class="accommodation-photo accommodation-photo-cover">
                        <img src="{{asset('app/images/sobi.jpg')}}">
                    </div>
                    <div class="accommodation-detail">
                        <h3>                                
                            <a href="rooms-en.html">Rooms</a>
                        </h3>
                        <p>
                           <span><i class="fas fa-check"></i>Bathroom</span>
                           <span><i class="fas fa-check"></i>Mini bar</span>
                           <span><i class="fas fa-check"></i>Terrace</span>
                           <span><i class="fas fa-check"></i>Free Wi-FI</span>
                           <span><i class="fas fa-check"></i>Air conditioning</span>
                        </p>
                        <div class="accommodation-read-more">
                            <a href="rooms-en.html" class="btn btn-primary">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6" data-aos="fade-up" data-aos-duration="2500">
                <div class="accommodation">
                    <div class="accommodation-photo accommodation-photo-cover">
                        <img src="{{asset('app/images/apartmani.jpg')}}">
                    </div>
                    <div class="accommodation-detail">
                        <h3>                                
                            <a href="apartments-en.html">Apartments</a>
                        </h3>
                        <p>
                           <span><i class="fas fa-check"></i>Kitchen</span>
                           <span><i class="fas fa-check"></i>Living Room</span>
                           <span><i class="fas fa-check"></i>Bedroom</span>
                           <span><i class="fas fa-check"></i>Mini bar</span>
                           <span><i class="fas fa-check"></i>Room service</span>
                        </p>
                        <div class="accommodation-read-more">
                            <a href="apartments-en.html" class="btn btn-primary">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6" data-aos="fade-up" data-aos-duration="3000">
                <div class="accommodation">
                    <div class="accommodation-photo accommodation-photo-cover">
                        <img src="{{asset('app/images/pretsedatelski.jpg')}}">
                    </div>
                    <div class="accommodation-detail">
                        <h3>                               
                            <a href="president-apartment-en.html">Presidential Suite</a>
                        </h3>
                        <p>
                           <span><i class="fas fa-check"></i>Two Bedrooms</span>
                           <span><i class="fas fa-check"></i>Kitchen</span>
                           <span><i class="fas fa-check"></i>Living Room</span>
                           <span><i class="fas fa-check"></i>Mini bar</span>
                           <span><i class="fas fa-check"></i>Room service</span>
                        </p>
                        <div class="accommodation-read-more">
                            <a href="president-apartment-en.html" class="btn btn-primary">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Footer -->

    @include('includes.footer-en')

    {{-- Preloader --}}

    <script src="{{asset('app/js/preloader.js')}}"> </script>

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>
</html>