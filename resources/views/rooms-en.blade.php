<!DOCTYPE html>
<html>
<head>
    <title>Rooms | Hotel Sileks</title>

    @include('includes.head-en')



</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->

    @include('includes.navbar-en')


    <!-- Room Header -->

    <div class="container-fluid">
        <div class="row room-header">
            <div class= "room-header-overlay">
                <div class="container">
                    <h1 class="header-text">Rooms</h1>
                </div>
            </div>
        </div>
    </div>


    <!-- Room Details -->

    <div class="container room-details-section">
        <div class="row">
            <div class="col-md-8 col-sm-12">                  
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                    <div class="item active" style="height: auto;">
                        <img src="{{asset('app/images/room.jpg')}}"  style="width:100%;">
                    </div>

                    <div class="item" style="height: auto;">
                        <img src="{{asset('app/images/room1.jpg')}}"  style="width:100%;">
                    </div>
                
                    <div class="item" style="height: auto;">
                        <img src="{{asset('app/images/room2.jpg')}}" style="width:100%;">
                    </div>
              
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>



                <div class="room-features">
                    <h6>Size: <span>20²</span></h6>
                    <h6>Capacity: <span>2</span></h6>
                    <h6>Bed: <span>Single/French bed</span></h6>
                    <h6>Services: <span>LCD, WI-FI...</span></h6>
                </div>


                <div class="room-details">
                    <p>Our rooms together with the unique surrounding environment connects with you, while ensuring your comfort and convenience. You will feel thoroughly at home, and completely at your best.</p>
                    <span><i class="fas fa-check"></i>Bathroom</span>
                    <span><i class="fas fa-check"></i>Terrace</span>
                    <span><i class="fas fa-check"></i>LCD ТV</span>
                    <span><i class="fas fa-check"></i>Free Wi-FI</span>
                    <span><i class="fas fa-check"></i>Cable TV Channels</span>
                    <span><i class="fas fa-check"></i>International direct telephone lines</span>
                    <span><i class="fas fa-check"></i>Air conditioning</span>
                    <span><i class="fas fa-check"></i>Mini bar</span>
                    <span><i class="fas fa-check"></i>Room service</span>
                    <span><i class="fas fa-check"></i>Wake up call</span>
                </div>
            </div>

            <!-- Accommodation filter -->

            <div class="col-md-4 col-sm-12">
                <section class="filter-area filter-area-mobile">
                    <div class="hotel-search-form-area" style="box-shadow: none !important; -webkit-transform: translateY(0);
                    -ms-transform: translateY(0);  transform: translateY(0); position: relative;">
                        <div class="container-fluid form-container" style="padding: 0 !important">
                            <div class="hotel-search-form">
                                <form action="#" method="post">
                                    <div class="row justify-content-between align-items-end filter-div">
                                        <div class="col-md-12">
                                            <label for="checkIn">From</label>
                                            <input type="date" class="form-control" id="checkIn" name="checkin-date">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="checkOut">To</label>
                                            <input type="date" class="form-control" id="checkOut" name="checkout-date">
                                        </div>
                                     <div class="col-md-6 ">
                                            <label for="adults">Persons</label>
                                            <select name="adults" id="adults" class="form-control">
                                                <option value="" disabled selected class="first-option">/</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                            </select>
                                        </div>                                         
                                        <div class="col-md-6 check-button">
                                            <label>/</label>
                                            <button type="submit" class="form-control btn check-avability">Check</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>                
            </div>
        </div>
    </div>


    <!-- Footer -->

    @include('includes.footer-en')

    {{-- Preloader --}}

    <script src="{{asset('app/js/preloader.js')}}"> </script>

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>
</html>