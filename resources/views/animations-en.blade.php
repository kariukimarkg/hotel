<!DOCTYPE html>
<html>
<head>
	<title>Animations | Hotel Sileks</title>

    @include('includes.head-en')

</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->

    @include('includes.navbar-en')



    <!-- Animations section -->

    <div class="container-fluid">
        <div class="row animation-header">
            <div class= "animation-header-overlay">
                <div class="container">
                    <h1 class="header-text">Animations</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid animation-section">
        <div class="row" data-aos="fade-up" data-aos-duration="1000">
            <div class="col-md-6" style="padding: 0">          
                <img src="{{asset('app/ images/bazen.jpg')}}"  style="width:100%;">
            </div>
            <div class="col-md-6 animation-description" data-aos="fade-up" data-aos-duration="2000">
                <h2>Pool</h2>
                <p>Hotel Sileks main attraction is the outdoor swimming pool where during the day and evening, guests have the opportunity to enjoy a variety of animation contents, such as water games, concerts, musical ensembles and theatre groups, summer beauty selection, all enriched with serving food and bevereges in the highest level.</p>
            </div>
        </div>
        <div class="row animation-item" data-aos="fade-up" data-aos-duration="2000">
            <div class="col-md-6 animation-description">
                <h2>Fitness Room</h2>
                <p>Whether you want to get in shape, improve your stamina, or just maintaining your condition our fitness room is always ready for you and is completely free.</p>
            </div>
            <div class="col-md-6" style="padding: 0">                  
              <img src="{{asset('app/images/fitness.jpg')}}"  style="width:100%;">
            </div>
        </div>
        <div class="row" data-aos="fade-up" data-aos-duration="3000">
            <div class="col-md-6"  style="padding: 0">          
                <img src="{{asset('app/images/sauna.jpg')}}"  style="width:100%;">
            </div>
            <div class="col-md-6 animation-description">
                <h2>Sauna</h2>
                <p>Refresh yourself with our sauna while improving your health until you are our guests.</p>
            </div>
        </div>
        <div class="row" data-aos="fade-up" data-aos-duration="2000">
            <div class="col-md-6 animation-description">
                <h2>Massage</h2>
                <p>For ultimate relaxation we offer you different types of professional massages in the comfort and privacy of your room or in the hotel’s massage salon or visit the beautifull beach afront of the hotel.</p>
            </div>
            <div class="col-md-6" style="padding: 0">                  
              <img src="{{asset('app/images/masaza.jpg')}}"  style="width:100%;">
            </div>
        </div>
    </div>


    <!-- Footer -->

    @include('includes.footer-en')


    <!-- Script preloader -->
    
	<script src="{{asset('app/js/preloader.js')}}"> </script>


    {{-- Script scroll fade in --}}

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>
</html>