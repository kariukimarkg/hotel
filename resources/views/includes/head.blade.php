<meta charset="UTF-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="keywords" content="hotel sileks, sileks, hotel, hotel ohrid, ohrid hotels, accommodation ohrid, appartment ohrid, rooms ohrid, pool ohrid, хотел, хотел охрид, сместување охрид, апартмани охрид, соби охрид, базен охрид, охрид, хотел силекс, силекс">
<meta name="description" content="Хотел Силекс **** се наоѓа во туристичкиот комплекс Св.Стефан, оддалечен 4км од центарот на Охрид, на регионалниот патен правец Охрид – Св.Наум и 10км од аеродромот Св.Апостол Павле.">
<meta name="author" content="Ненад Бошковски">


<meta property="og:type" content="website"/>
<meta property="og:title" content="Хотел Силекс"/>    
<meta property="og:description" content="Хотел Силекс **** се наоѓа во туристичкиот комплекс Св.Стефан, оддалечен 4км од центарот на Охрид, на регионалниот патен правец Охрид – Св.Наум и 10км од аеродромот Св.Апостол Павле."/>
<meta property="og:image" content="{{asset('app/images/carousel2.jpg')}}"/>
<meta property="og:url" content="https://hotelsileks.mk"/>





<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

<link rel="icon" type="image/ico" href="{{asset('app/images/logo.png')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app/css/style.css')}}">

<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
