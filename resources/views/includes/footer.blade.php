<footer class="footer">
    <div class="container footer-inner">
        <div class="row">
            <div class="col-md-3 col-md-offset-3 col-sm-6">
                <div class="footer-item">
                    <h4>Контакт</h4>
                    <div class="f-border"></div>
                    <ul class="contact-info">
                        <li>
                            <i class="fas fa-map-marker-alt"></i><span>Нас. Св. Стефан, Охрид, Македонија</span>
                        </li>
                        <li class="contact-info-meil">
                            <i class="far fa-envelope"></i><span><a href="mailto:info@hotelsileks.mk">info@hotelsileks.mk</a></span>
                        </li>
                        <li>
                            <i class="fas fa-phone"></i><span> ++389 46 277 300</span>
                        </li>
                        <li>
                            <i class="fas fa-fax"></i><span> ++ 389 46 277 304</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-1 col-sm-6">
                <div class="footer-item">
                    <h4>
                        Линкови
                    </h4>
                    <div class="f-border"></div>
                    <ul class="links">
                        <li>
                            <a href="{{route('index')}}">Почетна</a>
                        </li>
                        <li>
                            <a href="{{route('accommodation')}}">Сместување</a>
                        </li>
                        <li>
                            <a href="{{route('restaurant')}}">Ресторан</a>
                        </li>
                        <li>
                            <a href="{{route('seminars')}}">Семинари</a>
                        </li>
                        <li>
                            <a href="{{route('animations')}}">Анимации</a>
                        </li>
                        <li>
                            <a href="{{route('contact')}}">Контакт</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div>
            
        </div>
        <div class="row sub-footer">
            <div style="float:left;">
                <p style="margin-top: 10px">© 2020 Хотел Силекс</p>
            </div>
            <div class="social-list" style="float: right;">
                <a href="https://www.facebook.com/Hotel-Sileks-362233700579021/?ref=br_rs" class="facebook" target="blank" style="margin-right: 10px"><i class="fab fa-facebook-square"></i></a>
                <a href="https://www.instagram.com/hotel_sileks/" target="blank" class="instagram"><i class="fab fa-instagram"></i></a>
            </div>
        </div>
    </div>
</footer>