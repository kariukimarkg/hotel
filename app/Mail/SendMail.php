<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $cin;
    public $cout;
    public $adu;
    public $nam;
    public $ema;
    public $mes;
    public $la;

    public function __construct($checkin, $checkout, $adults, $name, $email, $message, $lang)
    {
        $this->cin = $checkin;
        $this->cout = $checkout;
        $this->adu = $adults;
        $this->nam = $name;
        $this->ema = $email;        
        $this->mes = $message;        
        $this->la = $lang;        
        
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $e_checkin = $this->cin;
        $e_checkout = $this->cout;
        $e_adults = $this->adu;
        $e_name = $this->nam;       
        $e_email = $this->ema;
        $e_message = $this->mes;
        $e_lang = $this->la;

        if ($e_lang != null) {
            return $this->view('email-en', compact("e_checkin", "e_checkout", "e_adults", "e_name", "e_email", "e_message"))->subject($e_message);
        } else {
            return $this->view('email', compact("e_checkin", "e_checkout", "e_adults", "e_name", "e_email", "e_message"))->subject($e_message);
        }


        

    }
}
