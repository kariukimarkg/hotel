<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\SendMail;

class FrontEndController extends Controller
{
    public function index(){
        return view('index');
    }
    public function accommodation(){
        return view('accommodation');
    }
    public function restaurant(){
        return view('restaurant');
    }
    public function seminars(){
        return view('seminars');
    }
    public function animations(){
        return view('animations');
    }
    public function contact(Request $request){
        $checkin = $request->checkin;
        $checkout = $request->checkout;
        $adults = $request->adults;

        $numPersons = ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6'];
        // dd($numPersons);
        return view('contact')->with(compact('checkin', 'checkout', 'adults', 'numPersons'));
    }
    public function rooms(){
        return view('rooms');
    }
    public function apartments(){
        return view('appartments');
    }
    public function pSuite(){
        return view('presidential-suite');
    }
    public function indexEn(){
        return view('index-en');
    }
    public function accommodationEn(){
        return view('accommodation-en');
    }
    public function roomsEn(){
        return view('rooms-en');
    }
    public function apartmentsEn(){
        return view('appartments-en');
    }
    public function pSuiteEn(){
        return view('presidental-suite-en');
    }
    public function restaurantEn(){
        return view('restaurant-en');
    }
    public function seminarsEn(){
        return view('seminars-en');
    }
    public function animationsEn(){
        return view('animations-en');
    }
    public function contactEn(Request $request){
        $checkin = $request->checkin;
        $checkout = $request->checkout;
        $adults = $request->adults;

        $numPersons = ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6'];
        // dd($numPersons);
        return view('contact-en')->with(compact('checkin', 'checkout', 'adults', 'numPersons'));
    }


    public function sendMail(Request $request){

        $this->validate($request, [
            'checkin' => 'date|nullable',
            'checkout' => 'date|after_or_equal:checkin|nullable',
            'adults' => 'numeric|nullable',
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        $checkin = $request->checkin;
        $checkout = $request->checkout;
        $adults = $request->adults;
        $name = $request->name;
        $email = $request->email;
        $message = $request->message;
        $lang = $request->lang;

        // dd($lang);

        Mail::to('info@hotelsileks.mk')->send(new SendMail($checkin, $checkout, $adults, $name, $email, $message, $lang));


        if ($request->has('lang')) {
            return back()->with('success', 'Ви благодариме што не контактиравте!');;
        }
        return back()->with('success', 'Thanks for contacting us!');
        
    }

}

